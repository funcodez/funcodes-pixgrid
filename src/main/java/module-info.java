module club.funcodes.pixgrid {
	requires org.refcodes.cli;
	requires org.refcodes.archetype;
	requires org.refcodes.graphical.ext.javafx;
	requires transitive org.refcodes.properties.ext.application;
	requires transitive javafx.graphics;

	exports club.funcodes.pixgrid;
}
