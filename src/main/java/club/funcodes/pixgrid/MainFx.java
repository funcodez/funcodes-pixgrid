// /////////////////////////////////////////////////////////////////////////////
// FUNCODES.CLUB
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package club.funcodes.pixgrid;

import static org.refcodes.cli.CliSugar.*;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.refcodes.archetype.CliHelper;
import org.refcodes.cli.ArgsFilter;
import org.refcodes.cli.ConfigOption;
import org.refcodes.cli.Example;
import org.refcodes.cli.Flag;
import org.refcodes.cli.NoneOperand;
import org.refcodes.cli.QuietFlag;
import org.refcodes.cli.Term;
import org.refcodes.data.AsciiColorPalette;
import org.refcodes.graphical.PixelShape;
import org.refcodes.graphical.RgbPixmap.RgbPixmapBuilder;
import org.refcodes.graphical.RgbPixmapBuilderImpl;
import org.refcodes.graphical.ext.javafx.FxGraphicalUtility;
import org.refcodes.graphical.ext.javafx.FxPixGridBannerPanel;
import org.refcodes.logger.RuntimeLogger;
import org.refcodes.logger.RuntimeLoggerFactorySingleton;
import org.refcodes.properties.ext.application.ApplicationProperties;
import org.refcodes.textual.Font;
import org.refcodes.textual.FontFamily;
import org.refcodes.textual.FontStyle;
import org.refcodes.textual.VerboseTextBuilder;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.scene.Scene;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

/**
 * Workaround for the FatJAR to work: Launch a <code>main()</code> method
 * which's class does *not* extend <code>Application</code> which then
 * statically calls this <code>MainFx</code>'s <code>main()</code> method. Why?
 * See"https://stackoverflow.com/questions/52653836/maven-shade-javafx-runtime-components-are-missing"
 */
public class MainFx extends Application {

	// /////////////////////////////////////////////////////////////////////////
	// STATICS:
	// /////////////////////////////////////////////////////////////////////////

	private static final RuntimeLogger LOGGER = RuntimeLoggerFactorySingleton.createRuntimeLogger();

	// /////////////////////////////////////////////////////////////////////////
	// CONSTANTS:
	// /////////////////////////////////////////////////////////////////////////

	private static final String APPLICATION_ICON = "application.png";
	private static final String NAME = "pixgrid";
	private static final String TITLE = "⚞PIX⩩GRID⚟";
	private static final String DEFAULT_CONFIG = NAME + ".ini";
	private static final char[] BANNER_PALETTE = AsciiColorPalette.MAX_LEVEL_GRAY.getPalette();
	private static final Font BANNER_FONT = new Font( FontFamily.DIALOG, FontStyle.BOLD );
	private static final String COPYRIGHT = "Copyright (c) by CLUB.FUNCODES (see [https://www.funcodes.club])";
	private static final String LICENSE_NOTE = "Licensed under GNU General Public License, v3.0 and Apache License, v2.0";
	private static final String DESCRIPTION = "Tool for displaying a light bulb matrix rendering configurable scrolling and fading pixmaps (PNG, GIF or JPG)";

	private static final String COLOR_NAME_BLACK = "Black";
	private static final String COLOR_NAME_DARK_GREY = "DarkGrey";

	public static final String PARAM_MATRIX_SCROLL_MAP_FILENAMES = "MATRIX_SCROLL_MAP_FILENAMES";
	public static final String PARAM_MATRIX_FADER_MAP_FILENAMES = "MATRIX_FADER_MAP_FILENAMES";
	public static final String PARAM_BANNER_IMAGE_FILE_PARAM_LIST = "BANNER_IMAGE_FILE_PARAM_LIST";
	public static final String PARAM_BACKGROUND_IMAGE_FILE = "BACKGROUND_IMAGE_FILE";
	public static final String PARAM_MATRIX_WIDTH = "MATRIX_WIDTH";
	public static final String PARAM_MATRIX_HEIGHT = "MATRIX_HEIGHT";
	public static final String PARAM_LEFT_BORDER = "LEFT_BORDER";
	public static final String PARAM_RIGHT_BORDER = "RIGHT_BORDER";
	public static final String PARAM_TOP_BORDER = "TOP_BORDER";
	public static final String PARAM_BOTTOM_BORDER = "BOTTOM_BORDER";
	public static final String PARAM_HORIZONTAL_SPACE = "HORIZONTAL_SPACE";
	public static final String PARAM_VERTICAL_SPACE = "VERTICAL_SPACE";
	public static final String PARAM_PIXEL_SHAPE = "PIXEL_SHAPE";
	public static final String PARAM_VALUE_PIXEL_SHAPE_RECTANGLE = "RECTANGLE";
	public static final String PARAM_VALUE_PIXEL_SHAPE_CIRCLE = "ELLIPSE";
	public static final String PARAM_PIXEL_WIDTH = "PIXEL_WIDTH";
	public static final String PARAM_PIXEL_HEIGHT = "PIXEL_HEIGHT";
	public static final String PARAM_BACKGROUND_COLOR = "BACKGROUND_COLOR";
	public static final String PARAM_INACTIVE_PIXEL_COLOR = "INACTIVE_PIXEL_COLOR";
	public static final String PARAM_MIN_PERPENDICULAR_DISTANCE = "MIN_PERPENDICULAR_DISTANCE";
	public static final String PARAM_MAX_PERPENDICULAR_DISTANCE = "MAX_PERPENDICULAR_DISTANCE";
	public static final String PARAM_MIN_PERPENDICULAR_DELAY = "MIN_PERPENDICULAR_DELAY";
	public static final String PARAM_MAX_PERPENDICULAR_DELAY = "MAX_PERPENDICULAR_DELAY";
	public static final String PARAM_MIN_DIAGONAL_DISTANCE = "MIN_DIAGONAL_DISTANCE";
	public static final String PARAM_MAX_DIAGONAL_DISTANCE = "MAX_DIAGONAL_DISTANCE";
	public static final String PARAM_MIN_DIAGONAL_DELAY = "MIN_DIAGONAL_DELAY";
	public static final String PARAM_MAX_DIAGONAL_DELAY = "MAX_DIAGONAL_DELAY";
	public static final String PARAM_MIN_DIRECTION_CHANGES = "MIN_DIRECTION_CHANGES";
	public static final String PARAM_MAX_DIRECTION_CHANGES = "MAX_DIRECTION_CHANGES";
	public static final String PARAM_MIN_FADER_MAP_SHOW_TIME = "MIN_FADER_MAP_SHOW_TIME";
	public static final String PARAM_MAX_FADER_MAP_SHOW_TIME = "MAX_FADER_MAP_SHOW_TIME";
	public static final String PARAM_DRAW_PIXEL_DELAY = "DRAW_PIXEL_DELAY";
	public static final String PARAM_DRAW_PIXEL_PROPABILITY = "DRAW_PIXEL_PROPABILITY";
	public static final String PARAM_PIXEL_BORDER_COLOR = "PIXEL_BORDER_COLOR";
	public static final String PARAM_PIXEL_BORDER_WIDTH = "PIXEL_BORDER_WIDTH";

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private int _matrixWidth;
	private int _matrixHeight;
	private int _pixelWidth;
	private int _pixelHeight;
	private PixelShape _pixelShape;
	private int _topBorder;
	private int _bottomBorder;
	private int _leftBorder;
	private int _rightBorder;
	private int _hoizontalSpace;
	private int _verticalSpace;
	private String _inactivePixelColor;
	private String _backgroundColor;
	private int _minPerpendicularDistance;
	private int _maxPerpendicularDistance;
	private int _minPerpendicularDelay;
	private int _maxPerpendicularDelay;
	private int _minDiagonalDistance;
	private int _maxDiagonalDistance;
	private int _minDiagonalDelay;
	private int _maxDiagonalDelay;
	private int _minDirectionChanges;
	private int _maxDirectionChanges;
	private int _minFaderMapShowTime;
	private int _maxFaderMapShowTime;
	private String[] _scrollMapFileNames;
	private String[] _faderMapFileNames;
	private Integer _drawPixelDelay;
	private Float _drawPixelPropability;
	private String _pixelBorderColor;
	private float _pixelBorderWidth;

	private List<RgbPixmapBuilder> _scrollMapColorArrayList = new ArrayList<>();
	private List<RgbPixmapBuilder> _faderMapColorArrayList = new ArrayList<>();
	private FxPixGridBannerPanel _pixGridPane;
	private Random _random = new Random();

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	public static void main( String args[] ) {
		launch( args );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void start( Stage aPrimaryStage ) {
		final NoneOperand theNoneArg = none( "Starts with the default configuration." );
		final Flag theInitFlag = initFlag( false );
		final ConfigOption theConfigOption = configOption();
		final Flag theSysInfoFlag = sysInfoFlag( false );
		final Flag theQuietFlag = quietFlag();
		final Flag theHelpFlag = helpFlag();
		final Flag theDebugFlag = debugFlag();

		// @formatter:off
		final Term theArgsSyntax = cases( 
			xor( theNoneArg, any( theConfigOption, theQuietFlag, theDebugFlag ) ), 
			and( theInitFlag, any( theConfigOption, theQuietFlag ) ),
			xor( theHelpFlag , and( theSysInfoFlag, any ( theQuietFlag ) ) )
		);
		final Example[] theExamples = examples(
			example( "Run (with a custom config in reach), be quiet", theQuietFlag ),
			example( "Initialize specific config file", theInitFlag, theConfigOption),
			example( "To show the help text", theHelpFlag ),
			example( "To print the system info", theSysInfoFlag )
		);
		
		final String[] theArgs = getParameters().getRaw() != null ? getParameters().getRaw().toArray( new String[ getParameters().getRaw().size()]) : new String[] {};
		final CliHelper theCliHelper = CliHelper.builder().
			// withArgs( theArgs ).
			withArgs( theArgs, ArgsFilter.D_XX ).
			withArgsSyntax( theArgsSyntax ).
			withExamples( theExamples ).
			withFilePath( DEFAULT_CONFIG ). // Must be the name of the default (template) configuration file below "/src/main/resources"
			withResourceClass( MainFx.class ).
			withName( NAME ).
			withTitle( TITLE ).
			withDescription( DESCRIPTION ).
			withLicense( LICENSE_NOTE ).
			withCopyright( COPYRIGHT ).
			withBannerFont( BANNER_FONT ).
			withBannerFontPalette( BANNER_PALETTE ).
			withLogger( LOGGER ).build();
		// @formatter:on

		final ApplicationProperties theArgsProperties = theCliHelper.getApplicationProperties();

		try {
			aPrimaryStage.getIcons().add( FxGraphicalUtility.toImage( Main.class.getResourceAsStream( "/" + APPLICATION_ICON ) ) );
			aPrimaryStage.setTitle( TITLE );
			run( theArgsProperties, aPrimaryStage );
		}
		catch ( Throwable e ) {
			theCliHelper.exitOnException( e );
		}
	}

	/**
	 * Runs the PixGRID main class.
	 * 
	 * @param aPrimaryStage The {@link Stage} used for drawing.
	 * @param aProperties The properties as evaluated by the
	 *        {@link #main(String[])} method.
	 */
	public void run( ApplicationProperties aProperties, Stage aPrimaryStage ) throws IOException {
		_matrixWidth = aProperties.getIntOr( PARAM_MATRIX_WIDTH, 80 );
		_matrixHeight = aProperties.getIntOr( PARAM_MATRIX_HEIGHT, 25 );
		_pixelWidth = aProperties.getIntOr( PARAM_PIXEL_WIDTH, 10 );
		_pixelHeight = aProperties.getIntOr( PARAM_PIXEL_HEIGHT, 10 );
		_pixelShape = aProperties.getEnumOr( PARAM_PIXEL_SHAPE, PixelShape.RECTANGLE );
		_topBorder = aProperties.getIntOr( PARAM_TOP_BORDER, 0 );
		_bottomBorder = aProperties.getIntOr( PARAM_BOTTOM_BORDER, 0 );
		_leftBorder = aProperties.getIntOr( PARAM_LEFT_BORDER, 0 );
		_rightBorder = aProperties.getIntOr( PARAM_RIGHT_BORDER, 0 );
		_hoizontalSpace = aProperties.getIntOr( PARAM_HORIZONTAL_SPACE, 0 );
		_verticalSpace = aProperties.getIntOr( PARAM_VERTICAL_SPACE, 3 );
		_inactivePixelColor = aProperties.getOr( PARAM_INACTIVE_PIXEL_COLOR, COLOR_NAME_DARK_GREY );
		_backgroundColor = aProperties.getOr( PARAM_BACKGROUND_COLOR, COLOR_NAME_BLACK );
		_minPerpendicularDistance = aProperties.getIntOr( PARAM_MIN_PERPENDICULAR_DISTANCE, 100 );
		_maxPerpendicularDistance = aProperties.getIntOr( PARAM_MAX_PERPENDICULAR_DISTANCE, 150 );
		_minPerpendicularDelay = aProperties.getIntOr( PARAM_MIN_PERPENDICULAR_DELAY, 15 );
		_maxPerpendicularDelay = aProperties.getIntOr( PARAM_MAX_PERPENDICULAR_DELAY, 40 );
		_minDiagonalDistance = aProperties.getIntOr( PARAM_MIN_DIAGONAL_DISTANCE, 25 );
		_maxDiagonalDistance = aProperties.getIntOr( PARAM_MAX_DIAGONAL_DISTANCE, 50 );
		_minDiagonalDelay = aProperties.getIntOr( PARAM_MIN_DIAGONAL_DELAY, 25 );
		_maxDiagonalDelay = aProperties.getIntOr( PARAM_MAX_DIAGONAL_DELAY, 75 );
		_minDirectionChanges = aProperties.getIntOr( PARAM_MIN_DIRECTION_CHANGES, 25 );
		_maxDirectionChanges = aProperties.getIntOr( PARAM_MAX_DIRECTION_CHANGES, 75 );
		_minFaderMapShowTime = aProperties.getIntOr( PARAM_MIN_FADER_MAP_SHOW_TIME, 1000 );
		_maxFaderMapShowTime = aProperties.getIntOr( PARAM_MAX_FADER_MAP_SHOW_TIME, 5000 );
		_scrollMapFileNames = aProperties.asArray( PARAM_MATRIX_SCROLL_MAP_FILENAMES );
		_faderMapFileNames = aProperties.asArray( PARAM_MATRIX_FADER_MAP_FILENAMES );
		_drawPixelDelay = aProperties.getIntOr( PARAM_DRAW_PIXEL_DELAY, 1 );
		_drawPixelPropability = aProperties.getFloatOr( PARAM_DRAW_PIXEL_PROPABILITY, 0.33F );
		_pixelBorderColor = aProperties.get( PARAM_PIXEL_BORDER_COLOR );
		_pixelBorderWidth = aProperties.getFloatOr( PARAM_PIXEL_BORDER_WIDTH, -1F );

		if ( !aProperties.getBoolean( QuietFlag.ALIAS ) ) {
			LOGGER.info( PARAM_MATRIX_WIDTH + " = " + _matrixWidth );
			LOGGER.info( PARAM_MATRIX_HEIGHT + " = " + _matrixHeight );
			LOGGER.info( PARAM_PIXEL_WIDTH + " = " + _pixelWidth );
			LOGGER.info( PARAM_PIXEL_HEIGHT + " = " + _pixelHeight );
			LOGGER.info( PARAM_PIXEL_BORDER_COLOR + " = " + _pixelBorderColor );
			LOGGER.info( PARAM_PIXEL_BORDER_WIDTH + " = " + _pixelBorderWidth );
			LOGGER.info( PARAM_PIXEL_SHAPE + " = " + _pixelShape );
			LOGGER.info( PARAM_TOP_BORDER + " = " + _topBorder );
			LOGGER.info( PARAM_BOTTOM_BORDER + " = " + _bottomBorder );
			LOGGER.info( PARAM_LEFT_BORDER + " = " + _leftBorder );
			LOGGER.info( PARAM_RIGHT_BORDER + " = " + _rightBorder );
			LOGGER.info( PARAM_HORIZONTAL_SPACE + " = " + _hoizontalSpace );
			LOGGER.info( PARAM_VERTICAL_SPACE + " = " + _verticalSpace );
			LOGGER.info( PARAM_INACTIVE_PIXEL_COLOR + " = " + _inactivePixelColor );
			LOGGER.info( PARAM_BACKGROUND_COLOR + " = " + _backgroundColor );
			LOGGER.info( PARAM_MIN_PERPENDICULAR_DISTANCE + " = " + _minPerpendicularDistance );
			LOGGER.info( PARAM_MAX_PERPENDICULAR_DISTANCE + " = " + _maxPerpendicularDistance );
			LOGGER.info( PARAM_MIN_PERPENDICULAR_DELAY + " = " + _minPerpendicularDelay );
			LOGGER.info( PARAM_MAX_PERPENDICULAR_DELAY + " = " + _maxPerpendicularDelay );
			LOGGER.info( PARAM_MIN_DIAGONAL_DISTANCE + " = " + _minDiagonalDistance );
			LOGGER.info( PARAM_MAX_DIAGONAL_DISTANCE + " = " + _maxDiagonalDistance );
			LOGGER.info( PARAM_MIN_DIAGONAL_DELAY + " = " + _minDiagonalDelay );
			LOGGER.info( PARAM_MAX_DIAGONAL_DELAY + " = " + _maxDiagonalDelay );
			LOGGER.info( PARAM_MIN_DIRECTION_CHANGES + " = " + _minDirectionChanges );
			LOGGER.info( PARAM_MAX_DIRECTION_CHANGES + " = " + _maxDirectionChanges );
			LOGGER.info( PARAM_MIN_FADER_MAP_SHOW_TIME + " = " + _minFaderMapShowTime );
			LOGGER.info( PARAM_MAX_FADER_MAP_SHOW_TIME + " = " + _maxFaderMapShowTime );
			LOGGER.info( PARAM_MATRIX_SCROLL_MAP_FILENAMES + " = " + VerboseTextBuilder.asString( _scrollMapFileNames ) );
			LOGGER.info( PARAM_MATRIX_FADER_MAP_FILENAMES + " = " + VerboseTextBuilder.asString( _faderMapFileNames ) );
			LOGGER.info( PARAM_DRAW_PIXEL_DELAY + " = " + _drawPixelDelay );
			LOGGER.info( PARAM_DRAW_PIXEL_PROPABILITY + " = " + _drawPixelPropability );
		}

		if ( _scrollMapFileNames != null ) {
			for ( String eFilename : _scrollMapFileNames ) {
				try {
					_scrollMapColorArrayList.add( new RgbPixmapBuilderImpl( new File( eFilename ) ) );
				}
				catch ( FileNotFoundException e ) {
					if ( eFilename != null && !eFilename.startsWith( "/" ) ) {
						try {
							_scrollMapColorArrayList.add( new RgbPixmapBuilderImpl( getClass().getResourceAsStream( "/" + eFilename ) ) );
						}
						catch ( Exception ignore ) {
							throw e;
						}
					}
					else {
						throw e;
					}
				}
			}
		}

		if ( _faderMapFileNames != null ) {
			for ( String eFilename : _faderMapFileNames ) {
				try {
					_faderMapColorArrayList.add( new RgbPixmapBuilderImpl( new File( eFilename ) ) );
				}
				catch ( FileNotFoundException e ) {
					if ( eFilename != null && !eFilename.startsWith( "/" ) ) {
						try {
							_faderMapColorArrayList.add( new RgbPixmapBuilderImpl( getClass().getResourceAsStream( "/" + eFilename ) ) );
						}
						catch ( Exception ignore ) {
							throw e;
						}
					}
					else {
						throw e;
					}
				}
			}
		}

		LOGGER.printTail();
		_pixGridPane = new FxPixGridBannerPanel( _matrixWidth, _matrixHeight, _backgroundColor, _pixelWidth, _pixelHeight, _pixelShape, _pixelBorderWidth, _pixelBorderColor, _inactivePixelColor, _topBorder, _bottomBorder, _leftBorder, _rightBorder, _hoizontalSpace, -1, false, _verticalSpace, -1, false );
		Scene theScene = new Scene( _pixGridPane, _pixGridPane.getWidth(), _pixGridPane.getHeight(), Color.BLACK );

		// _pixGridPane.setScaleX( 1.3 );
		// _pixGridPane.setScaleY( 1.3 );
		// _pixGridPane.getTransforms().add( new Rotate( 3.5 ) );

		_pixGridPane.setVisible( true );
		aPrimaryStage.setScene( theScene );
		aPrimaryStage.setResizable( false );
		aPrimaryStage.sizeToScene();

		aPrimaryStage.setOnCloseRequest( event -> Platform.exit() );

		if ( Platform.isFxApplicationThread() ) {
			aPrimaryStage.show();
		}
		else {
			Platform.runLater( new Runnable() {
				@Override
				public void run() {
					aPrimaryStage.show();
				}
			} );
		}
		Thread t = new Thread( () -> {
			while ( true ) {
				if ( !_faderMapColorArrayList.isEmpty() ) {
					displayFaderMap();
				}
				if ( !_scrollMapColorArrayList.isEmpty() ) {
					moveScrollMap();
				}
			}
		} );
		t.start();
	}

	// /////////////////////////////////////////////////////////////////////////
	// HELPER:
	// /////////////////////////////////////////////////////////////////////////

	private void moveScrollMap() {

		double eRandom;

		// ----------------------
		// Choose the scroll map:
		// ----------------------
		eRandom = _random.nextDouble() * _scrollMapColorArrayList.size();
		int theIndex = (int) eRandom;
		RgbPixmapBuilder theColorArray = _scrollMapColorArrayList.get( theIndex );

		// ---------------------------
		// Choose the initial offsets:
		// ---------------------------
		eRandom = _random.nextDouble() * ( theColorArray.getWidth() - 1 );
		int theX = (int) eRandom;

		eRandom = _random.nextDouble() * ( theColorArray.getHeight() - 1 );
		int theY = (int) eRandom;

		// --------------------
		// Fade the scroll map:
		// --------------------
		eRandom = _random.nextDouble() * 2;
		if ( (int) eRandom == 0 ) {
			_pixGridPane.stepFadeToPixmap( theColorArray, theX, theY, toRndStepWidth(), _drawPixelDelay );
		}
		if ( (int) eRandom == 1 ) {
			_pixGridPane.rndFadeToPixmap( theColorArray, theX, theY, _drawPixelPropability, _drawPixelDelay );
		}

		// ------------------------------------------
		// Calculate the number of direction changes:
		// ------------------------------------------
		eRandom = ( _random.nextDouble() * ( _maxDirectionChanges - _minDirectionChanges ) ) + _minDirectionChanges;

		int theLoops = (int) eRandom;

		// --------------------
		// Make the first move:
		// --------------------
		int eDirection = 2;
		int eFork;
		int eDiagonalSpeed;
		int ePerpendicularSpeed;
		int eDiagonalDistance;
		int ePerpendicularDistance;

		eRandom = ( _random.nextDouble() * ( _maxPerpendicularDelay - _minPerpendicularDelay ) ) + _minPerpendicularDelay;
		ePerpendicularSpeed = (int) eRandom;
		eRandom = ( _random.nextDouble() * ( _maxPerpendicularDistance - _minPerpendicularDistance ) ) + _minPerpendicularDistance;
		ePerpendicularDistance = (int) eRandom;
		_pixGridPane.moveEast( ePerpendicularDistance, ePerpendicularSpeed, ePerpendicularSpeed );

		for ( int i = 0; i < theLoops; i++ ) {

			eRandom = _random.nextDouble() * 2;
			eFork = (int) eRandom;

			eRandom = ( _random.nextDouble() * ( _maxDiagonalDelay - _minDiagonalDelay ) ) + _minDiagonalDelay;
			eDiagonalSpeed = (int) eRandom;
			eRandom = ( _random.nextDouble() * ( _maxDiagonalDistance - _minDiagonalDistance ) ) + _minDiagonalDistance;
			eDiagonalDistance = (int) eRandom;

			eRandom = ( _random.nextDouble() * ( _maxPerpendicularDelay - _minPerpendicularDelay ) ) + _minPerpendicularDelay;
			ePerpendicularSpeed = (int) eRandom;
			eRandom = ( _random.nextDouble() * ( _maxPerpendicularDistance - _minPerpendicularDistance ) ) + _minPerpendicularDistance;
			ePerpendicularDistance = (int) eRandom;

			// ----------
			// 0 = North:
			// ----------
			if ( eDirection == 0 ) {
				if ( eFork == 0 ) {
					eDirection = 7;
					_pixGridPane.moveNorthWest( ePerpendicularDistance, ePerpendicularSpeed, ePerpendicularSpeed );
				}
				if ( eFork == 1 ) {
					eDirection = 1;
					_pixGridPane.moveNorthEast( ePerpendicularDistance, ePerpendicularSpeed, ePerpendicularSpeed );
				}
			}
			else
				// ---------------
				// 1 = North-East:
				// ---------------
				if ( eDirection == 1 ) {
					if ( eFork == 0 ) {
						eDirection = 0;
						_pixGridPane.moveNorth( eDiagonalDistance, eDiagonalSpeed, eDiagonalSpeed );
					}
					if ( eFork == 1 ) {
						eDirection = 2;
						_pixGridPane.moveEast( eDiagonalDistance, eDiagonalSpeed, eDiagonalSpeed );
					}
				}
				else
					// ---------
					// 2 = East:
					// ---------
					if ( eDirection == 2 ) {
						if ( eFork == 0 ) {
							eDirection = 1;
							_pixGridPane.moveNorthEast( ePerpendicularDistance, ePerpendicularSpeed, ePerpendicularSpeed );
						}
						if ( eFork == 1 ) {
							eDirection = 3;
							_pixGridPane.moveSouthEast( ePerpendicularDistance, ePerpendicularSpeed, ePerpendicularSpeed );
						}
					}
					else
						// ---------------
						// 3 = South-East:
						// ---------------
						if ( eDirection == 3 ) {
							if ( eFork == 0 ) {
								eDirection = 2;
								_pixGridPane.moveEast( eDiagonalDistance, eDiagonalSpeed, eDiagonalSpeed );
							}
							if ( eFork == 1 ) {
								eDirection = 4;
								_pixGridPane.moveSouth( eDiagonalDistance, eDiagonalSpeed, eDiagonalSpeed );
							}
						}
						else
							// ----------
							// 4 = South:
							// ----------
							if ( eDirection == 4 ) {
								if ( eFork == 0 ) {
									eDirection = 3;
									_pixGridPane.moveSouthEast( ePerpendicularDistance, ePerpendicularSpeed, ePerpendicularSpeed );
								}
								if ( eFork == 1 ) {
									eDirection = 5;
									_pixGridPane.moveSouthWest( ePerpendicularDistance, ePerpendicularSpeed, ePerpendicularSpeed );
								}
							}
							else
								// ---------------
								// 5 = South-West:
								// ---------------
								if ( eDirection == 5 ) {
									if ( eFork == 0 ) {
										eDirection = 4;
										_pixGridPane.moveSouth( eDiagonalDistance, eDiagonalSpeed, eDiagonalSpeed );
									}
									if ( eFork == 1 ) {
										eDirection = 6;
										_pixGridPane.moveWest( eDiagonalDistance, eDiagonalSpeed, eDiagonalSpeed );
									}
								}
								else
									// ---------
									// 6 = West:
									// ---------
									if ( eDirection == 6 ) {
										if ( eFork == 0 ) {
											eDirection = 5;
											_pixGridPane.moveSouthWest( ePerpendicularDistance, ePerpendicularSpeed, ePerpendicularSpeed );
										}
										if ( eFork == 1 ) {
											eDirection = 7;
											_pixGridPane.moveNorthWest( ePerpendicularDistance, ePerpendicularSpeed, ePerpendicularSpeed );
										}
									}
									else
										// ---------------
										// 7 = North-West:
										// ---------------
										if ( eDirection == 7 ) {
											if ( eFork == 0 ) {
												eDirection = 6;
												_pixGridPane.moveWest( eDiagonalDistance, eDiagonalSpeed, eDiagonalSpeed );
											}
											if ( eFork == 1 ) {
												eDirection = 0;
												_pixGridPane.moveNorth( eDiagonalDistance, eDiagonalSpeed, eDiagonalSpeed );
											}
										}

		}
	}

	private void displayFaderMap() {
		// ----------------------
		// Choose the scroll map:
		// ----------------------
		int theIndex = toRndFaderPixmap();
		RgbPixmapBuilder theColorArray = _faderMapColorArrayList.get( theIndex );

		// --------------------
		// Fade the scroll map:
		// --------------------
		double eRandom = _random.nextDouble() * 3;
		if ( (int) eRandom == 0 ) {
			_pixGridPane.rndFadeToPixmap( theColorArray, 0, 0, _drawPixelPropability, _drawPixelDelay );
		}
		else {
			_pixGridPane.stepFadeToPixmap( theColorArray, 0, 0, toRndStepWidth(), _drawPixelDelay );
		}
		delay( toRndShowTime() );
	}

	/**
	 * @return
	 */
	private int toRndFaderPixmap() {
		double eRandom = _random.nextDouble() * _faderMapColorArrayList.size();
		int theIndex = (int) eRandom;
		return theIndex;
	}

	/**
	 * @param theMaxStep
	 * 
	 * @return
	 */
	private int toRndStepWidth() {
		int eRandom = (int) ( _random.nextDouble() * ( ( _pixGridPane.getMatrixHeight() * _pixGridPane.getMatrixWidth() ) - 1 ) );
		if ( eRandom < 1 ) {
			eRandom = 1;
		}
		return eRandom;
	}

	/**
	 * @return
	 */
	private double toRndShowTime() {
		return ( _random.nextDouble() * ( _maxFaderMapShowTime - _minFaderMapShowTime ) ) + _minFaderMapShowTime;
	}

	/**
	 * @param aDelayMillis
	 */
	private void delay( double aDelayMillis ) {
		try {
			Thread.sleep( (int) aDelayMillis );
		}
		catch ( InterruptedException ex ) {}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void stop() throws Exception {
		System.exit( 0 );
	}
}
